#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct{
char data[31];
struct Node* left;
struct Node* right;
}Node;
Node* getLeaf(Node* root,char* word);
Node* getMin(Node* root);
Node* getSuccessor(Node* root,char* word);
Node* getPredecessor(Node* root,char* word);
Node* newNode(char* word){
    Node*n=malloc(sizeof(Node));
    strcpy(n->data,word);
    n->left=n->right=NULL;
    return n;
}
Node* insert(Node* root,char* word){
    if(root==NULL)return(newNode(word));
    if(strcasecmp(word,root->data)==-1){
        root->left=insert(root->left,word);
    }else if(strcasecmp(word,root->data)==1){
        root->right=insert(root->right,word);
    }
    return root;
}
int getMaxHeight(Node* root){
    int x,y;
    if(root->left==NULL && root->right==NULL)return 0;
    if(root->left==NULL){
        return 1+getMaxHeight(root->right);
    }else if(root->right==NULL){
        return 1+getMaxHeight(root->left);
    }else{
        x=1+getMaxHeight(root->left);
        y=1+getMaxHeight(root->right);
        if(x>y){return x;}else{return y;}
    }
}
int search(Node* root,char* word){
    if(root==NULL) return 0;
    if(strcasecmp(word,root->data)==0) return 1;
    if(strcasecmp(word,root->data)==-1){
        if(search(root->left,word)){
            return 1;}else{ return 0;}
    }else{
        if(search(root->right,word)){
            return 1;}else{ return 0;}
    }
}
void printInorder(Node* root){
    if(root==NULL) return;
    printInorder(root->left);
    printf("%s\n",root->data);
    printInorder(root->right);
}
Node* getMin(Node* root){
    while(root->left!=NULL){
        root=root->left;
    }
    return root;
}
Node* getMax(Node* root){
    while(root->right!=NULL){
        root=root->right;
    }
    return root;
}
int readFile(FILE*fp,Node* root){
   int nooflines=0;
   char word[31];
   nooflines++;
   while(!feof(fp)){
        fscanf(fp,"%s",word);
        root=insert(root,word);
        nooflines++;
    }
   return nooflines;
}
void PrintSuggestions(Node* root,char* word){
    Node* temp1;
    temp1=getLeaf(root,word);
    printf("Suggestions: %s, ", temp1->data);
    temp1=getSuccessor(root,word);
    printf("%s, ", temp1);
    temp1=getPredecessor(root,word);
    printf("%s ", temp1);
}
Node* getLeaf(Node* root,char* word){
    if(strcasecmp(word,root->data)==-1){
        if(root->left==NULL){
            return root;
        }else{
        root=getLeaf(root->left,word);
    }}else{
        if(root->right==NULL){
            return root;
        }else{
        root=getLeaf(root->right,word);
    }
}return root;
}
Node* getSuccessor(Node* root,char* word){
    Node* current;
    current=getLeaf(root,word);
    if(current->right!=NULL){
        return getMin(current->right);
    }else{
        Node* successor=NULL;
        Node* ancestor=root;
        while(strcasecmp(current->data,ancestor->data)!=0){
            if(strcasecmp(current->data,ancestor->data)==-1){
            successor=ancestor;
            ancestor=ancestor->left;
        }else if(strcasecmp(current->data,ancestor->data)==1){
            ancestor=ancestor->right;
        }
      }
      return successor;
    }
}
Node* getPredecessor(Node* root,char* word){
    Node* current;
    current=getLeaf(root,word);
    if(current->left!=NULL){
        return getMax(current->left);
    }else{
        Node* predecessor=NULL;
        Node* ancestor=root;
        while(strcasecmp(current->data,ancestor->data)!=0){
            if(strcasecmp(current->data,ancestor->data)==-1){
            ancestor=ancestor->left;
        }else if(strcasecmp(current->data,ancestor->data)==1){
            predecessor=ancestor;
            ancestor=ancestor->right;
        }
      }
      return predecessor;
    }
}
int main()
{
    FILE*fp=fopen("Dictionary.txt","r");
    char word[31];
    fscanf(fp,"%s",word);
    Node* root= newNode(word);
    int size=readFile(fp,root);
    int height=getMaxHeight(root);
    printf("Dictionary Loaded Successfuly...!!\n..................................\nSize = %d\n..................................\n",size);
    printf("Height = %d\n..................................\nEnter a sentence :\n",height);
    char* line;
    gets(line);
    char* token;
    token=strtok(line," ");
    if(search(root,token)){
        printf("%s - Correct",token);
    }else{
        printf("%s - Incorrect   ",token);
        PrintSuggestions(root,token);
    }
    printf("\n");
    token=strtok(NULL," ");
    while(token!=NULL){
        if(search(root,token)){
            printf("%s - Correct",token);
        }else{
            printf("%s - Incorrect   ",token);
            PrintSuggestions(root,token);
        }
        printf("\n");
        token=strtok(NULL," ");
    }
    fclose(fp);
    return 0;
}
